
SELECT CustomerId, FirstName, LastName, sum(total) total
from Invoice
join Customer using (CustomerId)
group by CustomerId


SELECT CustomerId,FirstName, LastName,
	(
        SELECT SUM(Total)
        FROM Invoice i
        WHERE i.CustomerId = c.CustomerId
    ) AS 'total'
    
FROM Customer c;

