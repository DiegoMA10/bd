USE Chinook;
SELECT t.Name 
FROM Track t 
WHERE t.Milliseconds > (SELECT avg(Milliseconds) FROM Track);