SELECT e.FirstName , e.LastName , COUNT(c.CustomerId)
FROM Employee e
LEFT JOIN  Customer c ON e.EmployeeId=c.SupportRepId
GROUP BY e.EmployeeId;

SELECT FirstName, LastName,
    (
        SELECT COUNT(CustomerId)
        FROM Customer  c
        WHERE c.SupportRepId = e.EmployeeId
    ) AS 'Total'
FROM Employee e;
