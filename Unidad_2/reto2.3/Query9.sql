SELECT p.PlaylistId, t.* 
FROM Track t
JOIN PlaylistTrack p USING (TrackId)
WHERE PlaylistId = (
    SELECT PlaylistId
    FROM (
        SELECT PlaylistId, COUNT(*) AS canciones
        FROM PlaylistTrack
        GROUP BY PlaylistId
        ORDER BY canciones DESC
        LIMIT 1
    ) AS subquery
);
