use Chinook;
SELECT FirstName, 
    (SELECT Sum(
        (SELECT sum(i.Total)
			FROM Invoice i
            WHERE c.CustomerId = i.CustomerId) 
        ) AS Total
        FROM Customer  c
        WHERE c.SupportRepId = e.EmployeeId
    ) AS 'Total'
FROM Employee e;
