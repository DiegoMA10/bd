SELECT al.AlbumId, al.Title, ar.Name
FROM Album al
JOIN Artist ar ON al.ArtistId = ar.ArtistId
WHERE al.AlbumId IN 
	(SELECT AlbumId FROM Track 
			GROUP BY AlbumId HAVING COUNT(*) > 15);