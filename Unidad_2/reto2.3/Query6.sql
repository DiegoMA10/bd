SELECT AlbumId, canciones
FROM (SELECT AlbumId, COUNT(*) AS canciones
		FROM Track
		GROUP BY AlbumId) AS subquery1
WHERE canciones > (SELECT AVG(canciones)
		FROM (SELECT COUNT(*) AS canciones
				FROM Track
				GROUP BY AlbumId) AS subquery2
);
