use Chinook;
SELECT AlbumId , Title ,
        (SELECT COUNT(TrackId)
            FROM Track t
            WHERE a.AlbumId = t.AlbumId) as NumberSong
FROM Album a