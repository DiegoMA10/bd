SELECT AlbumId, SUM(Milliseconds) as total
FROM Track
GROUP BY AlbumId
HAVING total > (
    SELECT AVG(total)
		FROM (SELECT AlbumId, SUM(Milliseconds) total
				FROM Track
				GROUP BY AlbumId
    ) AS subquery
);
