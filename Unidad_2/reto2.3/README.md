# Reto 2.3: Consultas con subconsultas II
**[Bases de Datos] Unidad Didáctica 2: DML - Consultas Avanzadas**

Para este reto, volveremos a usar la base de datos Chinook (más información en el Reto 2.1).

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/DiegoMA10/bd/-/tree/main/Unidad_2/reto2.3?ref_type=heads

![Diagrama relacional de Chinook (fuente: github.com/lerocha/chinook-database).](https://github.com/lerocha/chinook-database/assets/135025/cea7a05a-5c36-40cd-84c7-488307a123f4)

Tras cargar esta base de datos en tu SGBD, realiza las siguientes consultas:

## Subconsultas Escalares (Scalar Subqueries)
Estas subconsultas devuelven un solo valor, por lo general, se utilizan en contextos donde se espera un solo valor, como parte de una condición `WHERE`, `SELECT`, `HAVING`, etc.
Ejemplo:

_Obtener una lista de empleados que ganan más que el salario medio de la empresa. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee
WHERE salary > (SELECT avg(salary)
                FROM employee)
```

### Consulta 1
Obtener las canciones con una duración superior a la media.
```sql
SELECT t.Name 
FROM Track t 
WHERE t.Milliseconds > (SELECT avg(Milliseconds) FROM Track);
```

### Consulta 2
Listar las 5 últimas facturas del cliente cuyo email es "emma_jones@hotmail.com".

```sql
SELECT * 
FROM Invoice
where CustomerId = (SELECT CustomerId FROM Customer where Email="emma_jones@hotmail.com")
ORDER BY InvoiceDate desc
limit 5;
```


## Subconsultas de varias filas

Diferenciamos dos tipos:

1. Subconsultas que devuelven una columna con múltiples filas (es decir, una lista de valores). Suelen incluirse en la cláusula `WHERE` para filtrar los resultados de la consulta principal. En este caso, suelen utilizarse con operadores como `IN`, `NOT IN`, `ANY`, `ALL`, `EXISTS` o `NOT EXISTS`.
2. Subconsultas que devuelven múltiples columnas con múltiples filas (es decir, tablas). Se comportan como una tabla temporal y se utilizan en lugares donde se espera una tabla, como en una cláusula `FROM`. [2]

### Consulta 3
Mostrar las listas de reproducción en las que hay canciones de reggae.
```sql
Select * from Playlist
WHERE PlaylistId IN (
	(SELECT PlaylistId FROM PlaylistTrack
		WHERE TrackId IN (SELECT TrackId FROM Track 
			WHERE GenreId IN (SELECT GenreId FROM Genre 
               WHERE Genre.Name = "reggae"))));

```


### Consulta 4
Obtener la información de los clientes que han realizado compras superiores a 20€.

```sql
SELECT DISTINCT * FROM Customer
WHERE CustomerId in (SELECT CustomerId FROM Invoice WHERE total > 20);
```

### Consulta 5
Álbumes que tienen más de 15 canciones, junto a su artista.

```sql
SELECT al.AlbumId, al.Title, ar.Name
FROM Album al
JOIN Artist ar ON al.ArtistId = ar.ArtistId
WHERE al.AlbumId IN 
	(SELECT AlbumId FROM Track 
			GROUP BY AlbumId HAVING COUNT(*) > 15);
```



### Consulta 6
Obtener los álbumes con un número de canciones superiores a la media.

```sql
SELECT AlbumId, canciones
FROM (SELECT AlbumId, COUNT(*) AS canciones
		FROM Track
		GROUP BY AlbumId) AS subquery1
WHERE canciones > (SELECT AVG(canciones)
		FROM (SELECT COUNT(*) AS canciones
				FROM Track
				GROUP BY AlbumId) AS subquery2
);

```

### Consulta 7
Obtener los álbumes con una duración total superior a la media.

```sql
SELECT AlbumId, SUM(Milliseconds) AS total_duration
FROM Track
GROUP BY AlbumId
HAVING SUM(Milliseconds) > (
    SELECT AVG(total_duration)
    FROM (
        SELECT AlbumId, SUM(Milliseconds) AS total_duration
        FROM Track
        GROUP BY AlbumId
    ) AS AlbumDurations
);

```

### Consulta 8
Canciones del género con más canciones.

```sql
select * 
from Track
where GenreId = 
		(Select GenreId 
		from (
			Select GenreId , count(*) as canciones
			from Track
			group by GenreId
			order by canciones desc
			limit 1
			) as subquery
	)
    
```

### Consulta 9
Canciones de la _playlist_ con más canciones.

```sql
SELECT p.PlaylistId, t.* 
FROM Track t
JOIN PlaylistTrack p USING (TrackId)
WHERE PlaylistId = (
    SELECT PlaylistId
    FROM (
        SELECT PlaylistId, COUNT(*) AS canciones
        FROM PlaylistTrack
        GROUP BY PlaylistId
        ORDER BY canciones DESC
        LIMIT 1
    ) AS subquery
);

```


## Subconsultas Correlacionadas (Correlated Subqueries):
Son subconsultas en las que la subconsulta interna depende de la consulta externa. Esto significa que la subconsulta se ejecuta una vez por cada fila devuelta por la consulta externa, suponiendo una gran carga computacional.
Ejemplo:

_Supongamos que queremos encontrar a todos los empleados con un salario superior al promedio de su departamento. [1]_

```sql
SELECT
  lastname,
  firstname,
  salary
FROM employee e1
WHERE e1.salary > (SELECT avg(salary)
                   FROM employee e2
                   WHERE e2.dept_id = e1.dept_id)
```

La principal diferencia entre una subconsulta correlacionada en SQL y una subconsulta simple es que las subconsultas correlacionadas hacen referencia a columnas de la tabla externa. En el ejemplo anterior, `e1.dept_id` es una referencia a la tabla de la subconsulta externa. [1]

### Consulta 10
Muestra los clientes junto con la cantidad total de dinero gastado por cada uno en compras.

```sql
SELECT CustomerId, FirstName, LastName, sum(total) total
from Invoice
join Customer using (CustomerId)
group by CustomerId
```

```sql
SELECT CustomerId,FirstName, LastName,
	(
        SELECT SUM(Total)
        FROM Invoice i
        WHERE i.CustomerId = c.CustomerId
    ) AS 'total'
    
FROM Customer c;
```

### Consulta 11
Obtener empleados y el número de clientes a los que sirve cada uno de ellos.

```sql
SELECT e.FirstName , e.LastName , COUNT(c.CustomerId)
FROM Employee e
LEFT JOIN  Customer c ON e.EmployeeId=c.SupportRepId
GROUP BY e.EmployeeId;
```

```sql
SELECT FirstName, LastName,
    (
        SELECT COUNT(CustomerId)
        FROM Customer  c
        WHERE c.SupportRepId = e.EmployeeId
    ) AS 'Total'
FROM Employee e;
```

### Consulta 12
Ventas totales de cada empleado.

```sql
SELECT FirstName, 
    (SELECT Sum(
        (SELECT sum(i.Total)
			FROM Invoice i
            WHERE c.CustomerId = i.CustomerId) 
        ) AS Total
        FROM Customer  c
        WHERE c.SupportRepId = e.EmployeeId
    ) AS 'Total'
FROM Employee e;
```

### Consulta 13
Álbumes junto al número de canciones en cada uno.

```sql
SELECT AlbumId , Title ,
        (SELECT COUNT(TrackId)
            FROM Track t
            WHERE a.AlbumId = t.AlbumId) as NumberSong
FROM Album a
```

### Consulta 14
Obtener el nombre del álbum más reciente de cada artista. Pista: los álbumes más antiguos tienen un ID más bajo.

```sql
use Chinook;
SELECT Name,Title
FROM  (SELECT ar.Name , 
	    (
        SELECT al.Title
        FROM Album al
        WHERE al.ArtistId = ar.ArtistId
        ORDER BY AlbumId
        LIMIT 1
	    )  AS Title
	FROM Artist ar
) AS subquery
WHERE Title IS NOT NULL
```

## Referencias
- [1] https://learnsql.es/blog/subconsulta-correlacionada-en-sql-una-guia-para-principiantes/
- [2] https://learnsql.es/blog/cuales-son-los-diferentes-tipos-de-subconsultas-sql/


<!--
HAVING ??
GROUP BY ??
-->
