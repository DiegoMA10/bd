use Chinook;
SELECT Name,Title
FROM  (SELECT ar.Name , 
		(SELECT al.Title
			FROM Album al
            WHERE al.ArtistId = ar.ArtistId
            ORDER BY AlbumId
            LIMIT 1
			)  AS Title
	FROM Artist ar
) AS subquery
WHERE Title IS NOT NULL
        