SELECT I.InvoiceDate AS "Fecha factura",
       CONCAT(C.FirstName ,' ', C.LastName) AS Nombre,
       C.Address AS "Dirección factura",
       C.PostalCode AS CP,
       C.Country AS Pais,
       I.Total  AS Cantidad
FROM Invoice I
JOIN Customer C ON I.CustomerId = C.CustomerId
WHERE C.City = 'Berlin';