SELECT concat(e1.FirstName,' ',e1.LastName) AS "Nombre Empleado",
	date(e1.BirthDate) as "Fecha nacimiento",
      concat(e2.FirstName,' ',e2.LastName) AS "Nombre supervisor"
FROM Employee AS e1
left JOIN Employee AS e2 ON e1.ReportsTo = e2.EmployeeId
ORDER BY e1.BirthDate DESC
LIMIT 15;