SELECT p.Name AS "Nombre playList",
       t.Name AS "Nombre cancion",
       a.Title AS "Nombre album",
       t.Milliseconds AS Duración
FROM Playlist p
JOIN PlaylistTrack pt ON p.PlaylistId = pt.PlaylistId
JOIN Track t ON pt.TrackId = t.TrackId
JOIN Album a ON t.AlbumId = a.AlbumId
WHERE p.Name LIKE 'C%'
ORDER BY a.Title, t.Milliseconds;