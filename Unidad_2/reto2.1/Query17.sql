SELECT a.Title AS Título, COUNT(a.Title) AS Numero_de_canciones
FROM Album a
JOIN Track t ON a.AlbumId = t.AlbumId
GROUP BY a.Title
ORDER BY Numero_de_canciones asc;