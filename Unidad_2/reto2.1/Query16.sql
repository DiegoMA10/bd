SELECT g.Name AS "Género", COUNT(*) AS "Número de canciones"
FROM Track t
JOIN Genre g ON t.GenreId = g.GenreId
GROUP BY g.Name;