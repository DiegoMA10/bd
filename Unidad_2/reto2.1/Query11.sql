SELECT DISTINCT c.FirstName, c.LastName
FROM Customer c
JOIN Invoice i ON c.CustomerId = i.CustomerId
WHERE i.Total > 10
ORDER BY c.LastName;