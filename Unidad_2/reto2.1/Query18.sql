-- top 5 generos mas vendidos 
Select g.Name , Count(*) as ventas
FROM Genre g 
JOIN Track t on g.GenreId = t.GenreId
JOIN InvoiceLine il on il.TrackId = t.TrackId
group by g.GenreId
order by ventas desc
limit 5