Reto 2.1: Consultas con funciones de agregación

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/DiegoMA10/bd/-/tree/main/Unidad_2/reto2.1?ref_type=heads

## Consultas sobre una tabla

### Query 1
Encuentra todos los clientes de Francia.

```sql
SELECT * FROM Customer WHERE Country = "France";
```

### Query 2
Muestra las facturas del primer trimestre de este año.

```sql
SELECT * 
FROM Invoice
WHERE YEAR(InvoiceDate) = YEAR(curdate()) AND MONTH(InvoiceDate) <= 3;
```

### Query 3
Muestra todas las canciones compuestas por AC/DC.

```sql
SELECT Track.Name 
FROM Track 
Where Composer = "AC/DC";
```

### Query 4
Muestra las 10 canciones que más tamaño ocupan.

```sql
SELECT Track.name , Bytes
FROM Track 
ORDER BY Bytes DESC
LIMIT 10;
```

### Query 5
Muestra el nombre de aquellos países en los que tenemos clientes.

```sql
SELECT DISTINCT Country 
FROM Chinook.Customer;
```

### Query 6
Muestra todos los géneros musicales.

```sql
SELECT Name as genero
FROM Chinook.Genre;
```


## Consultas sobre múltiples tablas

### Query 7
Muestra todos los artistas junto a sus álbumes.

```sql
SELECT AR.Name AS artista , AL.Title AS album 
FROM Artist AS AR 
join Album AS AL ON AR.ArtistId = AL.ArtistId;
```

### Query 8
Muestra los nombres de los 15 empleados más jóvenes junto a los nombres de
sus supervisores, si los tienen.

```sql
SELECT concat(e1.FirstName,' ',e1.LastName) AS "Nombre Empleado",
	date(e1.BirthDate) as "Fecha nacimiento",
      concat(e2.FirstName,' ',e2.LastName) AS "Nombre supervisor"
FROM Employee AS e1
left JOIN Employee AS e2 ON e1.ReportsTo = e2.EmployeeId
ORDER BY e1.BirthDate DESC
LIMIT 15;
```

### Query 9
Muestra todas las facturas de los clientes berlineses. Deberán mostrarse las colum-
nas: fecha de la factura, nombre completo del cliente, dirección de facturación,
código postal, país, importe (en este orden).


```sql
SELECT I.InvoiceDate AS "Fecha factura",
       CONCAT(C.FirstName ,' ', C.LastName) AS Nombre,
       C.Address AS "Dirección factura",
       C.PostalCode AS CP,
       C.Country AS Pais,
       I.Total  AS Cantidad
FROM Invoice I
JOIN Customer C ON I.CustomerId = C.CustomerId
WHERE C.City = 'Berlin';


```

### Query 10
Muestra las listas de reproducción cuyo nombre comienza por C, junto a todas
sus canciones, ordenadas por álbum y por duración.

```sql
SELECT p.Name AS "Nombre playList",
       t.Name AS "Nombre cancion",
       a.Title AS "Nombre album",
       t.Milliseconds AS Duración
FROM Playlist p
JOIN PlaylistTrack pt ON p.PlaylistId = pt.PlaylistId
JOIN Track t ON pt.TrackId = t.TrackId
JOIN Album a ON t.AlbumId = a.AlbumId
WHERE p.Name LIKE 'C%'
ORDER BY a.Title, t.Milliseconds;


```
### Query 11
Muestra qué clientes han realizado compras por valores superiores a 10€, orde-
nados por apellido.

```sql
SELECT DISTINCT c.FirstName, c.LastName
FROM Customer c
JOIN Invoice i ON c.CustomerId = i.CustomerId
WHERE i.Total > 10
ORDER BY c.LastName;

```

## Consultas con funciones de agregación

### Query 12
Muestra el importe medio, mínimo y máximo de cada factura.

```sql
SELECT avg(Total) AS media ,min(Total) AS Min, max(Total) AS Max
FROM Invoice

```

### Query 13
Muestra el número total de artistas.

```sql
SELECT count(*) as "Artistas totales"
FROM Artist;

```

### Query 14
Muestra el número de canciones del álbum “Out Of Time”.

```sql
SELECT a.Title ,count(*) as "Numero de canciones"
FROM Track t JOIN Album a
ON t.AlbumId=a.AlbumId
GROUP BY a.AlbumId
HAVING a.Title = 'Out Of Time'

```

### Query 15
Muestra el número de países donde tenemos clientes.

```sql
SELECT COUNT(DISTINCT Country) as "Numero de paises"
FROM Customer
```

### Query 16
Muestra el número de canciones de cada género (deberá mostrarse el nombre del
género).

```sql
SELECT g.Name AS "Género", COUNT(*) AS "Número de canciones"
FROM Track t
JOIN Genre g ON t.GenreId = g.GenreId
GROUP BY g.Name;
```

### Query 17
Muestra los álbumes ordenados por el número de canciones que tiene cada uno.

```sql
SELECT a.Title AS Título, COUNT(a.Title) AS Numero_de_canciones
FROM Album a
JOIN Track t ON a.AlbumId = t.AlbumId
GROUP BY a.Title
ORDER BY Numero_de_canciones asc;
```

### Query 18
Encuentra los géneros musicales más populares (los más comprados).

```sql
-- top 5 generos mas vendidos 
Select g.Name , Count(*) as ventas
FROM Genre g 
JOIN Track t on g.GenreId = t.GenreId
JOIN InvoiceLine il on il.TrackId = t.TrackId
group by g.GenreId
order by ventas desc
limit 5
```

### Query 19
Lista los 6 álbumes que acumulan más compras.

```sql
SELECT a.Title ,count(*) ventas
FROM Album a
JOIN Track t on a.AlbumId = t.AlbumId
Join InvoiceLine il on il.TrackId = t.TrackId 
group by a.Title
order by ventas desc
limit 6

```

### Query 20
Muestra los países en los que tenemos al menos 5 clientes.

```sql
SELECT Country, COUNT(Country)
FROM Customer
GROUP BY Country
HAVING COUNT(Country) >= 5;

```
