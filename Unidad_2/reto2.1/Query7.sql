SELECT AR.Name AS artista , AL.Title AS album 
FROM Artist AS AR 
join Album AS AL ON AR.ArtistId = AL.ArtistId;