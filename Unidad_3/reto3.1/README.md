# Reto 1: DDL - Definición de datos en MySQL

Diego Muñoz Arribas

En esta práctica vamos a utilizar y hablar sobre la creación de la base de datos `reserva.sql`.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/DiegoMA10/bd/-/tree/main/Unidad_3/reto3.1

## Crear base de datos
Para crear una base de datos utilizaremos:

```sql
CREATE SCHEMA "reserva";
```
También se puede utilizar:
```sql
CREATE DATABASE "reserva";
```

## Crear tablas en una base de datos
Para crear una tabla utilizaremos el comando `CREATE TABLE`:

En la tabla pasajeros crearemos dos columnas `numero_pasaporte` y `nombre_pasajero` la clave primaria es el numero de pasaporte.
```sql
USE reserva;
CREATE TABLE `pasajeros` (
  `numero_pasaporte` varchar(20) NOT NULL,
  `nombre_pasajero` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`numero_pasaporte`));
```
En la tabla vuelos creamos varias columnas y la mas curiosa de todas es la de `capacidad` que es un tipo de dato `SMALLINT` esto quiere decir que su numeros es entre -32768 y 32767, pero haciendo que el tipo de dato sea `UNSIGNED` hace que no pueda tener valores negativos utilizandos los bits negativos para el positivo, ahora los numeros son entre puede ser entre 0 y 65535.
```sql
USE reserva;
CREATE TABLE `vuelos` (
  `id_vuelo` varchar(10) NOT NULL,
  `origen` varchar(50) NOT NULL,
  `destino` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `capacidad` SMALLINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id_vuelo`));
```

La tabla de vuelos_pasajeros es la tabla que utilizaremos para relacionar las tablas de `vuelos` y `pasajeros` y asegurar la integridad de los datos.
```sql
USE reserva;
CREATE TABLE `vuelos_pasajeros` (
  `id_vuelo` varchar(10) NOT NULL,
  `pasaporte` varchar(20) NOT NULL,
  `asiento` varchar(3) NOT NULL,
  `id_reserva` varchar(10) NOT NULL,
  PRIMARY KEY (`id_reserva`));
```

## Modificar una tabla

Primero de todo para modifcar una tabla utilizaremos `ALTER TABLE`, podemos alterar una tabla para añadir `FOREING KEY` , modificar algun tipo de dato, cambiar el nombre a las columnas o tanto borrar como crear nuevas columnas, etc...

En esta consulta creamos dos foreing key una en la columna `pasaporte` que referencia el numero_pasaporte de la tabla pasajeros, y la otra `id_vuelo` que referencia el id_vuelo de la tabla vuelos.

- El `ADD CONSTRAINT` sirve para crear una restricción después de que ya se haya creado una tabla.


```sql
ALTER TABLE `reserva`.`vuelos_pasajeros` 
ADD CONSTRAINT `pasaporte`
    FOREIGN KEY (`pasaporte`)
    REFERENCES `reserva`.`pasajeros` (`numero_pasaporte`),
ADD CONSTRAINT `id_vuelo`
    FOREIGN KEY (`id_vuelo`)
    REFERENCES `reserva`.`vuelos` (`id_vuelo`);


```
### Mostrar información base de datos
Para mostrar información podemos utilizar `SHOW`:

En este caso muestra las bases de datos del sistema:
```sql
SHOW DATABASES;
```

En este muestra las tablas de la base de datos que estes utilizando:
```sql
SHOW TABLES;
```

Y por ultimo concluir mostrar más en específico los datos de cada columna, esto mostrar que tipo de datos son: 
```sql
SHOW COLUMNS FROM pasajeros
```

### TCL

El `AUTOCOMMIT` sirve para que en cada transacción realize como una especie de backup, se utiliza en momentos que tengamos que modificar las tablas de base de datos para asegurarse de que en cualquier error que podamos tener asegurar la integridad de los datos.

Para utilizarlo hay que setear el `AUTOCOMMIT` A `FALSE` y cuando realizamos un `COMMIT` aseguramos ya la permanencia de los datos.
```sql
SET AUTOCOMMIT ="FALSE"
COMMIT;
```

Para volver en el momento que seteamos el `AUTOCOMMIT` en caso de que nos hayamos equivocado utilizaremos el siguiente comando:

```sql
ROLLBACK;
```

### Subconsultas

Podemos hacer subcolutas en este caso borraremos todas las reservas donde la condicion sea los resultados de otra consulta:
```sql
DELETE FROM Reservas
WHERE pasaporte IN (
    SELECT pasaporte FROM Pasajeros
    WHERE nombre LIKE "Elsa%");
```



## PREGUNTAS

¿Por qué necesitamos tres tablas?

Necesitamos tres tablas de datos una para los `pasajeros` otra para los `vuelos` y la ultima "`vuelos_pasajeros`" es porque necesitamos una manera de relacionar las dos tablas que tenemos y por ello utilizaremos una tabla para relacionar estas dos tablas entre ellas.

¿Cuáles son las claves primarias y foráneas?

- En la tabla `pasajeros` la clave primaria es el pasaporte.
- En la tabla `vuelos` la clave primaria es id_vuelo.
- En la tabla `vuelos_pasajeros` la clave primaria es id_reserva y esta tabla tiene dos claves foráneas una es id_vuelo que se referencia en la clave primaria de la tabla vuelos y la otra pasaporte que se referencia en la clave primaria de la tabla pasajeros.

¿Por qué utilizamos las restricciones que hemos definido y no otras?

Utilizamos las `claves primarias` y las `claves foráneas` para asegurarnos de que los datos no se puedan repetir y que coincidan con los datos que hay en las tablas.

También hacemos que algunos datos sean `NOT NULL` para asegurarnos de que en esos campos no sean nulos.

### Reflexiones

Si intentas borrar un vuelo y ese vuelo esta asociado a una reserva te va a saltar un error de que no se puede eliminar el registro porque se esta utilizando en otra tabla con una clave foránea por lo tanto para poder eliminar el vuelo no tendria que existir ninguna relacion de ese vuelo con una reserva.

Para que dos pasajeros no puedan estar asignados a un mismo asiento podemos hacer que cada asiento sea unico o con la restriccion `UNIQUE` o haciendo que sea una clave primaria.

En caso de que un pasajero se asigne a un vuelo no existente no podras crear una reserva y saltara un error, porque en la reserva se necesita que haya un vuelo existente para poder ser creada por lo tanto no se le puede asignar un vuelo que no exista.

Otras situaciones donde se puede utilizar este tipo de restricciones es como por ejemplo en sistemas bancarios no tendria sentido transferir dinero a una cuenta que no existe o tener dos cuentas con la misma id de dos personas distinas.

