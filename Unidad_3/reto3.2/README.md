# RETO 3.2 DCL Control de acceso

Diego Muñoz Arribas

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/DiegoMA10/bd/-/tree/main/Unidad_3/reto3.2

**Explora las opciones de control de acceso que ofrece MySQL. Al menos, deberemos documentar:** 

- Cómo registrar nuevos usuarios, modificarlos y eliminarlos.
-  Cómo se autentican los usuarios y qué opciones de autenticación nos ofrece el SGBD.
-    Cómo mostrar los usuarios existentes y sus permisos.
-    Qué permisos puede tener un usuario y qué nivel de granularidad nos ofrece el SGBD.
-    Qué permisos hacen falta para gestionar los usuarios y sus permisos.
-    Posibilidad de agrupar los usuarios (grupos/roles/...).
-    Qué comandos usamos para gestionar los usuarios y sus permisos.



### Conexión a mysql

Para conectarnos a la base de datos desde terminar podremos utilizar el siguiente comando os distintos parametros sirven para: la `-P` se utiliza para el puerto, la `-u` es el usuario, `-p` con este te pedira la contraseña para acceder y por ultimo `-h` el host donde se deseas conectarte.

```bash
mysql -P 33006 -u root -p -h 127.0.0.1
```

### Ver bases de datos

Para mostrar una lista de todas las bases de datos disponibles , puedes utilizar el comando `SHOW DATABASES`,este comando mostrara todas las base de datos las cuales tengas permiso de ver.

```sql
SHOW DATABASES;
```

### Mostrar usuarios

Para ver la lista de usuarios registrados, puedes utilizar el siguiente comando SQL:

```sql
SELECT user FROM mysql.user;
```

### mostrar privilegios usuario

Para ver los privilegios concedidos a un usuario específico , puedes utilizar el siguiente comando SQL:

```sql
show grants for 'root'@'%';
```
Este comando muestra los privilegios otorgados al usuario 'root' desde cualquier host ('%'). Proporciona una lista detallada de los permisos que tiene el usuario sobre diferentes bases de datos y tablas, así como los tipos de privilegios específicos que se le han concedido.

Como por ejemplo:

1. SELECT: Permite al usuario seleccionar datos de una tabla.
2. INSERT: Permite al usuario agregar nuevos registros a una tabla.
3. UPDATE: Permite al usuario modificar registros existentes en una tabla.
4. DELETE: Permite al usuario eliminar registros de una tabla.
5. CREATE: Permite al usuario crear nuevas bases de datos y tablas.
6. DROP: Permite al usuario eliminar bases de datos y tablas existentes.
7. GRANT OPTION: Permite al usuario otorgar o revocar privilegios a otros usuarios.
8. ALL PRIVILEGES: Concede todos los privilegios disponibles en una base de datos o tabla específica.
9. USAGE: Permite al usuario conectarse a la base de datos, pero no tiene ningún privilegio específico.
10. EXECUTE: Permite al usuario ejecutar funciones o procedimientos almacenados.

### crear usuario

Para crear un nuevo usuario , puedes utilizar el siguiente comando SQL:

```sql
CREATE USER 'prueba'@'%' IDENTIFIED BY '123';
```

Este comando crea un nuevo usuario llamado ``prueba`` y le asigna una contraseña ``123``. El ``%`` indica que este usuario puede conectarse desde cualquier host. Puedes cambiar ``%`` por una dirección IP específica o un nombre de host si quieres limitar desde dónde puede conectarse el usuario como por ejemplo:

```sql
CREATE USER 'prueba'@'192.168.4.%' IDENTIFIED BY '123';
```

### borrar usuario
Para eliminar un usuario , puedes utilizar el siguiente comando SQL:

```sql
DROP USER 'nuevo_usuario'@'%';
```
Es importante tener cuidado al eliminar usuarios, ya que esta acción es irreversible

### cambiar host y nombre

Si necesitas cambiar el host desde el cual un usuario puede conectarse o el nombre de usuario, puedes utilizar el siguiente comando SQL:

```sql
RENAME USER 'nuevo_usuario'@'%' TO 'nuevo_usuario'@'192.168.4.%';
```
Este comando cambia el host del usuario 'nuevo_usuario' de cualquier host ``%`` a uno específico ``192.168.4.%``. Esto restringe la conexión del usuario solo a direcciones IP que comienzan con ``192.168.4``.

En el caso de que también quisiéramos cambiar el nombre:
```sql
RENAME USER 'nuevo_usuario'@'%' TO 'usuario_cambiado'@'192.168.4.%';
```


### dar permisos para ver una tabla

Si quieres dar permisos a un usuario para que pueda ver los datos de una tabla específica en una base de datos, puedes utilizar el siguiente comando SQL:

```sql
GRANT SELECT ON Chinook.(nombre_tabla) TO 'nuevo_usuario'@'%';
```
El permiso SELECT permite al usuario ver los datos de la tabla especificada, pero no realizar modificaciones en ella. Si Quieres dar otros privilegios, como INSERT, UPDATE, o DELETE, puedes especificarlos en lugar de SELECT.

Por ejemplo:
```sql
GRANT INSERT ON Chinook.(nombre_tabla) TO 'nuevo_usuario'@'%';
```

### quitar permisos para ver una tabla
Si quieres quitar los permisos otorgados previamente a un usuario para ver los datos de una tabla específica en una base de datos, puedes utilizar el siguiente comando SQL:
```sql
REVOKE SELECT(PERMISSION_TYPE) ON Chinook.(tabla) FROM 'nuevo_usuario'@'%';
```

Es importante tener en cuenta que este comando afecta únicamente al permiso SELECT en la tabla especificada y no revoca ningún otro permiso adicional que pueda haber sido otorgado al usuario.

### Crear un rol

En MySQL, los roles son una forma de agrupar privilegios para usuarios. Puedes crear un nuevo rol utilizando el siguiente comando SQL:

```sql
CREATE ROLE 'usuario_raso';
```
Este comando crea un nuevo rol llamado ``usuario_raso``. Los roles pueden ser útiles para dar los mismos privilegios a varios usuarios a la vez.


### Dar privilegios a un rol

```sql
GRANT SELECT ON Chinook.* TO "usuario raso"@'%';
```

### Ver los privilegios de un rol

```sql
 show grants for 'usuario raso'@'%';
```

### Dar roles a usuarios

```sql
GRANT 'usuario raso' TO 'nuevo_usuario'@'%';
```

### activar el rol

```sql
SET ROLE 'usuario raso';
```

### saber que rol tienes

```sql
SELECT CURRENT_ROLE();
```

### rol por defecto
```sql
SET DEFAULT ROLE  "usuario raso"  TO "hola";
```

### bloquear cuenta

```sql
ALTER USER "ivan" ACCOUNT LOCK;
```

### Activar cuenta

```sql
ALTER USER "ivan" ACCOUNT UNLOCK;
```



### Matar sesion
```sql
SELECT id FROM  information_schema.processlist WHERE user = "ivan";
KILL (numero_id);
```
Enlaces de interés:

    https://www.hostinger.es/tutoriales/como-crear-usuario-mysql
    https://dev.mysql.com/doc/refman/8.3/en/access-control.html

