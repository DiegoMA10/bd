# Unidad C0: Recapitulación

Diego Muñoz Arribas

**Este documento ofrece una recapitulación sobre conceptos esenciales de bases de datos, el modelo cliente-servidor, el lenguaje SQL y las bases de datos relacionales.**

## Concepto y origen de las bases de datos
**¿Qué son las bases de datos?**

Es un sistema organizado de almacenamiento de datos que permite la
gestión, recuperación, actualización y análisis de información de manera eficiente.

**¿Qué problemas tratan de resolver?** 

Problemas relacionados con la gestion y manipulación de datos

**Definición de base de datos.**

Conjunto de datos organizado de tal modo que permita obtener con rapidez diversos tipos de información.

## Sistemas de gestión de bases de datos
**¿Qué es un sistema de gestión de bases de datos (DBMS)?**

Es un software que se utiliza para gestionar y administrar bases de datos.

**¿Qué características de acceso a los datos debería proporcionar?**

Proporciona características como un lenguaje de consulta (SQL), soporte para transacciones para garantizar la integridad de los datos, índices para mejorar la velocidad de búsqueda, vistas para simplificar consultas y mejorar la seguridad, procedimientos almacenados para la lógica de negocio, control de acceso para la seguridad de los datos y optimización de consultas para mejorar el rendimiento.

**Definición de DBMS.**

Es un software que permite a los usuarios almacenar, gestionar y manipular grandes volúmenes de datos de manera eficiente y segura.

### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB:
Se usa hoy en dia , cliente-servidor, software privado
* IBM Db2:
Se usa pero es menos frecuente, cliente-servidor,software privado
* SQLite:
Se usa hoy en dia, cliente,software libre
* MariaDB:
Se usa hoy en dia, cliente-servidor,software libre
* SQL Server:
Se usa hoy en dia, cliente-servidor, software privado
* PostgreSQL:
Se usa hoy en dia, cliente-servidor, software libre
* mySQL:
Se usa hoy en dia, cliente-servidor, software libre

## Modelo cliente-servidor
**¿Por qué es interesante que el DBMS se encuentre en un servidor?**

Porque centraliza los datos y permite el acceso remoto, simplificando la gestión y garantizando la disponibilidad de la información.

**¿Qué ventajas tiene desacoplar al DBMS del cliente?**

Ofrece la independencia de la plataforma, mayor seguridad, mejor escalabilidad y administración centralizada de los datos.

**¿En qué se basa el modelo cliente-servidor?**
Se basa en la comunicación entre dos entidades: el cliente, que solicita servicios o recursos, y el servidor, que los proporciona.

* __Cliente__: Entidad que solicita servicios o recursos al servidor.
* __Servidor__: Entidad que proporciona servicios o recursos a los clientes.
* __Red__: Medio de comunicación que permite la interacción entre clientes y servidores.
* __Puerto de escucha__: Número de identificación asociado a un servicio en el servidor, donde este escucha las solicitudes entrantes de los clientes.
* __Petición__: Solicitud enviada por el cliente al servidor para solicitar un servicio o recurso.
* __Respuesta__: Respuesta enviada por el servidor al cliente en respuesta a una petición, proporcionando el servicio o recurso solicitado.

## SQL
**¿Qué es SQL?**

Structured Query Language, es un lenguaje de programación utilizado para administrar y manipular bases de datos relacionales.

**¿Qué tipo de lenguaje es?**

Es un lenguaje de consulta estándar para bases de datos relacionales y permite realizar diversas operaciones, como consultar datos, insertar, actualizar y eliminar registros, así como administrar la estructura de la base de datos.

### Instrucciones de SQL

#### DDL (Data Definition Language)

Se utiliza para definir, modificar y eliminar la estructura de objetos en una base de datos, como tablas, índices, vistas, etc.

#### DML (Data Manipulation Language)

Se utiliza para manipular datos en una base de datos, incluyendo operaciones como insertar, actualizar, eliminar y recuperar registros en tablas.

#### DCL (Data Control Language)

Se utiliza para controlar los privilegios de acceso y seguridad en una base de datos, incluyendo operaciones como otorgar, revocar permisos y controlar la integridad de los datos.

#### TCL (Transaction Control Language)

Se utiliza para administrar transacciones en una base de datos, incluyendo operaciones para confirmar o deshacer transacciones, así como para establecer puntos de guardado.

## Bases de datos relacionales

**¿Qué es una base de datos relacional?**

Una base de datos relacional es un sistema de gestión de bases de datos que organiza los datos en tablas con filas y columnas. Cada tabla representa una entidad o relación en el mundo real, y cada fila en la tabla corresponde a un registro específico de esa entidad. Los datos se relacionan entre sí a través de claves primarias y claves foráneas.

**¿Qué ventajas tiene?** 

1. Estructura Organizada: Facilita la organización y gestión de los datos.

2. Flexibilidad: Permite consultas complejas y relaciones entre datos de diferentes tablas.

3. Integridad de los Datos: Garantiza la integridad de los datos mediante restricciones de integridad referencial.

4. Seguridad: Ofrece sistemas de control de acceso y permisos para proteger los datos sensibles.

5. Escalabilidad: Puede manejar grandes volúmenes de datos y crecer con las necesidades de la aplicación.

¿Qué elementos la conforman?

* __Relación (tabla)__: Almacena datos organizados en filas y columnas.
* __Atributo/Campo (columna)__: Representa una característica o propiedad de los datos.
* __Registro/Tupla (fila)__: Contiene los valores específicos de cada entidad o instancia de datos.

