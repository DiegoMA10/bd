# Reto 1: Consultas básicas

Diego Muñoz Arribas

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/DiegoMA10/bd/-/tree/main/Unidad_1/sanitat

## Query 1


Esta consulta SQL extrae información específica de la tabla HOSPITAL. Selecciona el ``HOSPITAL_COD``, ``NOM`` y ``TELEFON`` de todos los hospitales registrados en la base de datos.

```sql
use sanitat;
select h.HOSPITAL_COD , h.NOM , h.TELEFON from HOSPITAL h ;
```


## Query 2

Esta consulta SQL selecciona el ``HOSPITAL_COD``, ``NOM`` y ``TELEFON`` de los hospitales cuyo nombre la segunda letra sea la  "a" y cualquier cantidad de caracteres adicionales. Utilizando  ``WHERE`` con el operador ``LIKE`` y el patrón `"_a%"` para filtrar los resultados.

```sql
use sanitat;
select h.HOSPITAL_COD , h.NOM , h.TELEFON 
from HOSPITAL h
where h.NOM like "_a%";
```

## Query 3

La consulta selecciona los campos ``HOSPITAL_COD``, ``SALA_COD``, ``EMPLEAT_NO`` y ``COGNOM`` de la tabla ``PLANTILLA``.

```sql
use sanitat;
select p.HOSPITAL_COD , p.SALA_COD , p.EMPLEAT_NO , p.COGNOM
from PLANTILLA p;
```

## Query 4

Esta consulta SQL selecciona los campos `HOSPITAL_COD`, `SALA_COD`, `EMPLEAT_NO`, `COGNOM` y `TORN` de la tabla PLANTILLA. Sin embargo, solo recupera las filas donde el valor del campo `TORN` no es igual a "N".

```sql
use sanitat;
select p.HOSPITAL_COD , p.SALA_COD , p.EMPLEAT_NO , p.COGNOM ,p.TORN
from PLANTILLA p
where p.TORN != "N";
```

## Query 5

Esta consulta SQL selecciona todas las columnas de la tabla `MALALT` de la base de datos. Filtra las filas para incluir solo aquellas en las que el año de nacimiento del paciente sea 1960.

```sql
use sanitat;
select *
from MALALT m
where year(m.DATA_NAIX)= 1960;
```

## Query 6

Esta consulta SQL selecciona todas las columnas de la tabla `MALALT` de la base de datos. Filtra las filas para incluir solo aquellas en las que el año de nacimiento del paciente sea igual o posterior a 1960.

```sql
use sanitat;
select *
from MALALT m
where year(m.DATA_NAIX) >= 1960;
```