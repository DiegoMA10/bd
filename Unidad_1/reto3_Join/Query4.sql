USE videoclub;
SELECT p.Titol AS Pelicula, a.Nom AS Actor
FROM PELICULA p
INNER JOIN INTERPRETADA i 
INNER JOIN ACTOR a 
ON i.CodiActor = a.CodiActor 
AND p.CodiPeli = i.CodiPeli;