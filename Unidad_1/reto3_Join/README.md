# Reto 3: Consultas JOIN

Diego Muñoz Arribas

En este reto trabajamos con la base de datos `videoclub`, que nos viene dada en el fichero  `videoclub.sql` . A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/DiegoMA10/bd/-/tree/main/Unidad_1/reto3_Join 


## VIDEOCLUB:

![Foto de la base de datos de Videoclub](https://gitlab.com/DiegoMA10/bd/-/raw/main/Unidad_1/reto2/videoclub.png)


### Query 1:
 
Esta consulta muestra los campos de `Titol` , `codiPeli` de la tabla `PELICULA` y `Descripción` de la tabla `GENERE` relacionando las dos tablas con un `INNER JOIN` relacionando el `codiGenere`.

```sql
USE videoclub;
SELECT p.codiPeli, p.Titol,g.Descripcio AS genero
FROM PELICULA p
INNER JOIN GENERE g ON p.codiGenere=g.codiGenere;
```

### Query 2:

Esta consulta muestra todos los campos de la tabla `FACTURA` y el `Nom` de la tabla `CLIENT` relacionando los `DNI` con un `INNER JOIN`, donde el nombre tiene que ser `Maria`.

```sql
USE videoclub;
SELECT f.* , c.Nom
FROM FACTURA f
INNER JOIN CLIENT c ON f.DNI=c.DNI
WHERE c.Nom LIKE "Maria%";
```

### Query 3:

Esta consulta muestra el `titol` de la tabla `PELICULA` y `Nom` de la tabla `ACTOR` relacionando los `codiActor` de ambas tablas utilizando `INNER JOIN`.

```sql
USE videoclub;
SELECT p.titol AS "Pelicula",a.Nom AS "Actor principal"
FROM PELICULA p
INNER JOIN ACTOR a ON p.codiActor=a.codiActor;
```

### Query 4:

Esta consulta muestra el ``titol`` de la tabla de ``PELICULA`` junto con los ``Nom`` de la tabla  ``ACTOR`` que han participado en ellas utilizando la tabla intermedia ``INTERPRETADA``, para la relación, se utilizan dos ``INNER JOIN``.

```sql
USE videoclub;
SELECT p.Titol AS Pelicula, a.Nom AS Actor
FROM PELICULA p
INNER JOIN INTERPRETADA i ON p.CodiPeli = i.CodiPeli
INNER JOIN ACTOR a ON i.CodiActor = a.CodiActor;
```

Otra manera:

```sql
USE videoclub;
SELECT p.Titol AS Pelicula, a.Nom AS Actor
FROM PELICULA p
INNER JOIN INTERPRETADA i 
INNER JOIN ACTOR a 
ON i.CodiActor = a.CodiActor 
AND p.CodiPeli = i.CodiPeli;
```

### Query 5:

Esta consulta muestra el ``codiPeli`` y ``Titol`` de dos películas relacionadas entre sí en la tabla ``PELICULA``. Para relacionarlas, utilizaremos la columna ``SegonaPart``, que indica cuál es la segunda parte de la película.

```sql
USE videclub;
SELECT p1.CodiPeli, p1.Titol, p2.CodiPeli, p2.Titol
FROM PELICULA p1
INNER JOIN PELICULA p2 ON p1.SegonaPart = p2.CodiPeli;
```
