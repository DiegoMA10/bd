# Reto 2: Consultas 

Diego Muñoz Arribas

En este reto trabajamos con la base de datos `empresa` y `videoclub`, que nos viene dada en el fichero `empresa.sql` y `videoclub.sql` . A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/DiegoMA10/bd/-/tree/main/Unidad_1/reto2


## EMPRESA:

![Foto de base de datos de la empresa](https://gitlab.com/DiegoMA10/bd/-/raw/main/Unidad_1/reto2/Empresa.png)


### Query 1:

Esta consulta muestra los campos de ``PROD_NUM`` , ``DESCRIPCIO`` de la tabla ``PRODUCTE``.
```sql
use empresa;
select PROD_NUM , DESCRIPCIO
from PRODUCTE;
```

### Query 2:

Esta consulta muestra los campos de ``PROD_NUM`` , ``DESCRIPCIO`` de la tabla ``PRODUCTE``, Pero con la condición de que en el campo ``DESCRIPCION`` tiene que contener la palabra tennis.

```sql
use empresa;
select PROD_NUM, DESCRIPCIO
from PRODUCTE
where DESCRIPCIO like "%tennis%";
```

### Query 3:

Esta consulta muestra los campos de ``CLIENT_COD``, ``NOM``, ``AREA``, ``TELEFON`` de la tabla ``CLIENT``.

```sql
use empresa;
select CLIENT_COD, NOM, AREA, TELEFON
from CLIENT;
```

### Query 4:

Esta consulta muestra los campos de ``CLIENT_COD``, ``NOM``, ``CIUTAT`` de la tabla ``CLIENT`` , Pero con la condición que en el campo ``AREA`` no contiene el número 636.

```sql
use empresa;
select CLIENT_COD, NOM, CIUTAT
from CLIENT
where AREA!=636;
```

### Query 5:

Esta consulta muestra los campos de ``COM_NUM``, ``COM_DATA`` ,``DATA_TRAMESA`` de la tabla ``COMANDA``.

```sql
use empresa;
select COM_NUM, COM_DATA ,DATA_TRAMESA
from COMANDA;
```

## VIDEOCLUB:

![Foto de la base de datos de Videoclub](https://gitlab.com/DiegoMA10/bd/-/raw/main/Unidad_1/reto2/videoclub.png)

### Query 6:

Esta consulta muestra los campos de ``Nom`` , ``Telefon`` de la tabla ``CLIENT``.

```sql
use videoclub;
select Nom , Telefon
from CLIENT;
```

### Query 7:

Esta consulta muestra los campos de ``Data`` , ``Import`` de la tabla ``FACTURA``.

```sql
use videoclub;
select Data , Import
from FACTURA;
```

### Query 8:

Esta consulta muestra el campo de ``Descripcio`` de la tabla ``DETALLFACTURA``, Pero con la condición de que el ``CodiFactura`` sea el número 3.

```sql
use videoclub;
select Descripcio
from DETALLFACTURA
where CodiFactura=3;
```

### Query 9:

Esta consulta muestra todos los campos de la tabla ``FACTURA`` de una manera descendiente teniendo en cuenta el valor de ``Import``.

```sql
use videoclub;
select *
from FACTURA
order by import desc;
```

### Query 10:

Esta consulta muestra el campo de ``Nom`` de la tabla ``ACTOR`` , Pero con la condición de que en ``Nom`` comience por la letra X.

```sql
use videoclub;
select Nom
from ACTOR
where  nom like "X%" ;
```
 
## Querys Adicionales

## VIDEOCLUB:

### Query 11:
**Añade dos actores que empiezan por la letra X.**  

Esta consulta insertara en la tabla `ACTOR` el `Nom` y el `CodiActor` que le indiques en el `values`.

```sql
insert into ACTOR
values (4 , "Ximo");
```
```sql
insert into ACTOR
values (5, "Xavi");
```
`Insert` cambiando el orden de las columnas para insertar los datos.
```sql
insert into ACTOR (Nom, CodiActor) 
values ("Xordi", 6);

```


### Query 12:
**Elimina el actor con el codigo 6.**

Esta consulta borrar de la tabla `ACTOR` la fila donde el `CodiActor` sea 6.

```sql
delete from ACTOR 
where CodiActor = 6; 
``` 

### Query 13:
**Elimina a varios actores.**  

Esta consulta borrar las filas de la tabla `ACTOR` donde el `CodiActor` sea 4 y 5.

```sql
delete from ACTOR
where CodiActor = 4 or CodiActor = 5;
```
Otra manera para eliminar a varios actores utilizando `IN`.

```sql
delete from ACTOR 
where CodiActor in (4, 5)
```

### Quary 14:
**Añade 2 actores a la vez.** 

Esta consulta es lo mismo que en la [*query 11*](#query-11) con la diferencia de que en esta Query se insertan dos filas a la vez.

```sql

insert into ACTOR (Nom, CodiActor)
values ("Xordi", 6), ("Xavi", 7);

```

### Query 15:
**Cambiale el nombre a Xordi por Jordi sin eliminarlo.**  

```sql
update ACTOR
set Nom = "Jordi"
where CodiActor = 6;
```

## EMPRESA:

### Query 16:
**¿En qué años hemos dado de alta trabajadores?**

Esta consulta mostrara de la tabla `EMP` los distintos años donde los trabajadores se han dado de alta, con la ayuda de `YEAR` para que muestre solo el año y también `distinct` para que solo muestre los distintos.

```sql
Select distinct YEAR(DATA_ALTA)
From EMP;
```

### Query 17:
**¿Qué categorias de empleados tengo actualmente?** 

Esta consulta muestra el campo `OFICI` de la tabla `EMP` y con la ayuda de `distinct` solo mostrara los que son distintos.

```sql
select distinct OFICI
from EMP;
```

En esta consulta contara el numero de `OFICI` que hay distintos.

**¿Cuántas son en total?**  

```sql
select count(distinct OFICI)
from EMP;
```

### Query 18:
**¿Qué empleados van a comisión?** 

En esta consulta mostrara todos los campos de la tabla `EMP` con la condición de que en el campo `COMISSIO` no sea nullo.

Si en vez de `IS NOT NULL`, pusieramos `IS NULL` solo mostraria los que son nullos.

```sql
select *
from EMP
where COMISSIO IS NOT NULL ;
```

### Query 19:
**¿Qué dos empleados se llevan la mayor comisión?**  

Esta consulta es lo mismo que en la [*anterior*](#query-18) con la diferencia de que Ordenaremos el campo de `COMISSIO` de manera descendente y solo mostrara los primeros 2 con la ayuda de `limit 2`.
```sql
select *
from EMP
where COMISSIO IS NOT NULL
order by COMISSIO desc
limit 2;

```
